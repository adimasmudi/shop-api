﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Models
{
    public partial class ShopContext : DbContext
    {
        public ShopContext()
        {
        }

        public ShopContext(DbContextOptions<ShopContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                _ = optionsBuilder.UseSqlServer(@"Server=.;Database=Shop;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _ = modelBuilder.Entity<Products>(entity =>
            {
                _ = entity.HasKey(e => e.ProductId)
                    .HasName("PK__Products__B40CC6CD237E8213");

                _ = entity.Property(e => e.Category)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                _ = entity.Property(e => e.Color)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                _ = entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                _ = entity.Property(e => e.UnitPrice).HasColumnType("decimal(18, 0)");
            });

            _ = modelBuilder.Entity<Users>(entity =>
            {
                _ = entity.HasKey(e => e.UserId)
                    .HasName("PK__Users__1788CC4C397167C7");


                _ = entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                _ = entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                _ = entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                _ = entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                _ = entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
